# ltlcal001-3088-RPiHAT-project

A git exercise to practice setting up a version control system for a project. EEE3088F 2021. LTLCAL001

## UPS uHAT Description

This design project aims to detail a design of a microPi HAT (uHAT) compliant PCB of an
uninterrupted power supply (UPS) “daughter board” for a Raspberry Pi (RPi) Zero single board
computer (SBC). The uHAT is made up of several submodules that enable it to provide power to
a RPi Zero when there is no power being supplied to the RPi by the mains power supply.

The use cases are related to the common practice of setting up a RPi as a home server, network
attached storage (NAS) system, as part of an arrangement with RPi add-ons with mobility for
example a home-surveillance or a dash-cam camera system and general physical computing tasks
with breadboards and software.

The UPS uHAT will attach directly to (below / underneath) the RPi Zero with all RPi input/output
(I/O) access points still available. The UPS uHAT will be affixed and will connect to the RPI via
the bottom of certain general-purpose I/O (GPIO) pins on the RPi. The scenario this UPS uHAT
will be used in is when there is loss of mains power and the RPi Zero device or system must either
continue operating or have enough power to shutdown correctly.


## UPS uHAT Design Project Requirements

### This design project aims to detail a design of a UPS uHAT compliant PCB to practice:
- Creating specifications and requirements
- Using modular design techniques and simulation
- Designing to a standard
- Documenting a design
- Testing a design

The use of a UPS uHAT potentially solves the problem of losing certain function(s) during a power
outage. It may be damaging to a computer’s storage device (or attached storage devices) to lose
power suddenly. This may occur during a power outage, load shedding or circuit-breaker trip.
There may be damage to projects and data especially if the device(s) are being used at the time of
the loss of power.

### The UPS uHAT design is a PCB generated using KiCad consisting of 3 submodule circuits
simulated in LTspice:

1. A switching power regulator (voltmeter or SHUNT & signal opamp to constrain Voltages
as 0-3.3V)
2. Three status LEDs
GREEN -FULL-
RED -BAT-
BLUE -CHARGE-
3. Power circuitry to convert battery voltage to voltages good for the RPi via GPIO pins.
---
### The complete UPS uHAT design also consists of:
- Battery connectors: input signal from uHAT CHARGE signal <- RPi via uHAT to charge
battery & output signal BAT signal for battery discharge -> RPi
- Zero Volt Diode (ZVD) circuit to interface CHARGE & BAT: a synchronous rectifier
circuit where a power MOSFET acts as a low voltage drop diode that switches at 0V – used
to conduct negative current from Vdd -> Vss
- GPIO port, (40pin RPi compliant header) to connect to uHAT -> RPi power and data
signals via RPi GPIO pins
- USB micro-B 5V input plug for input power signal
- CP2104 Serial Chip to interface code and commands from the RPi to the uHAT
- MAX 17040 Fuel Gauge compact, low-cost host-side fuel-gauge system for Li+ batteries

These submodules would work with a Lithium Ion (Li+) battery all mounted and affixed
underneath the RPi Zero to provide power to the RPi device should the mains power be out.
The design process models the idea of an existing UPS HAT. The format follows specific design
processes in 3 user scenarios. This format emulates a user deciding to design their own UPS uHAT
for a specific use. This format may also emulate a set of design cases where certain needs must bemet for the design to work for a client base, should one decide on one of these or any other use
case scenario to be widely used.

Designing a product that can work for non-specific scenarios. So PCB design is general.
This section will outline design requirements for the UPS uHAT PCB. 3 Scenarios are detailed
where the base UPS uHAT design can be incorporated as part of a larger RPi Zero implementation.
The UPS uHAT is designed by the user in each scenario. The UPS uHAT device can be
implemented for these scenarios as well as many more, this is illustrated by the specificity in each
user design case.

The design scenarios are outlined, detailed and specific requirements are given for each. General
requirements for the design project are distilled from the 3 scenarios and apply to each as the base
UPS uHAT design. These requirements are furthered in the next section regarding specifications.
